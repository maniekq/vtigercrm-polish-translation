<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'SINGLE_Calendar' => 'Uwagi',
	'LBL_ADD_TASK' => 'Dodaj Uwagi',
	'LBL_ADD_EVENT' => 'Dodaj Czynność',
	'LBL_RECORDS_LIST' => 'Lista wydarzeń',
	'LBL_RECORD_SUMMARY' => 'Podsumowanie wydarzenia',
	'LBL_EVENTS' => 'Czynności',
	'LBL_TODOS' => 'Zadania',
	'LBL_EVENT_OR_TASK' => 'Wydarzenie / Uwagi',

	// Blocks
	'LBL_TASK_INFORMATION' => 'Szczegóły Uwagi',
    'LBL_EVENT_INFORMATION'=> 'Szczegóły wydarzenia',

	//Fields
	'Subject' => 'Temat',
	'Start Date & Time' => 'Data rozpoczęcia',
	'Activity Type'=>'Typ czynności',
	'Send Notification'=>'Wyślij powiadomienie',
	'Location'=>'Lokalizacja',
	'End Date & Time' => 'Data zakończenia',

	//Side Bar Names
	'LBL_ACTIVITY_TYPES' => 'Typ czynności',
	'LBL_CONTACTS_SUPPORT_END_DATE' => 'Data zakończenia wsparcia',
	'LBL_CONTACTS_BIRTH_DAY' => 'Data urodzin',

	//Activity Type picklist values
	'Call' => 'Telefon',
	'Meeting' => 'Spotkanie',

	//Status picklist values
	'Planned' => 'Planowany',
        'Held'    => 'Held',
        'Not Held'=> 'Nie Held',
	'Completed' => 'Zakończone',
	'Pending Input' => 'W realizacji',
	'Not Started' => 'Nierozpoczęte',
	'Deferred' => 'Przełożone',

	//Priority picklist values
	'Medium' => 'Normalny',

	'LBL_CHANGE_OWNER' => 'Zmień właściciela',

	'LBL_EVENT' => 'Czynność',
	'LBL_TASK' => 'Uwagi',
	'LBL_TASKS' => 'Uwagi',

	'LBL_CALENDAR_VIEW' => 'Widok kalendarza',
	
	'Mobile Call' => 'Mobile Call',
	
	//Fixing colors for Shared Calendar and My Calendar
	'LBL_EDIT_COLOR' => 'Kolor Edit',
	'LBL_ADD_CALENDAR_VIEW' => 'Dodaj kalendarz',
	'LBL_SELECT_USER_CALENDAR' => 'Wybierz kalendarz użytkownika',
	'LBL_SELECT_CALENDAR_COLOR' => 'Wybierz kolor kalendarz',
	'LBL_EDITING_CALENDAR_VIEW' => 'Montaż kalendarza View',
	'LBL_DELETE_CALENDAR' => 'Usuń kalendarz',
	'LBL_SELECT_ACTIVITY_TYPE' => 'Wybierz typ aktywny',
	'Tasks' => 'Zadania',

);

$jsLanguageStrings = array(

	'LBL_ADD_EVENT_TASK' => 'Dodaj zadanie / Uwagi',
    
	//Calendar view label translation
	'LBL_MONTH' => 'Miesiąc',
	'LBL_TODAY' => 'Dzisiaj',
	'LBL_DAY' => 'Dzień',
	'LBL_WEEK' => 'Tydzień',
	
	'LBL_SUNDAY' => 'Niedziela',
	'LBL_MONDAY' => 'Poniedziałek',
	'LBL_TUESDAY' => 'Wtorek',
	'LBL_WEDNESDAY' => 'Środa',
	'LBL_THURSDAY' => 'Czwartek',
	'LBL_FRIDAY' => 'Piątek',
	'LBL_SATURDAY' => 'Sobota',
	
	'LBL_SUN' => 'Nd',
	'LBL_MON' => 'Pon',
	'LBL_TUE' => 'Wt',
	'LBL_WED' => 'Śr',
	'LBL_THU' => 'Czw',
	'LBL_FRI' => 'Pt',
	'LBL_SAT' => 'Sob',
	
	'LBL_JANUARY' => 'Styczeń',
	'LBL_FEBRUARY' => 'Luty',
	'LBL_MARCH' => 'Marzec',
	'LBL_APRIL' => 'Kwiecień',
	'LBL_MAY' => 'Maj',
	'LBL_JUNE' => 'Czerwiec',
	'LBL_JULY' => 'Lipiec',
	'LBL_AUGUST' => 'Sierpień',
	'LBL_SEPTEMBER' => 'Wrzesień',
	'LBL_OCTOBER' => 'Październik',
	'LBL_NOVEMBER' => 'Listopad',
	'LBL_DECEMBER' => 'Grudzień',
	
	'LBL_JAN' => 'Sty',
	'LBL_FEB' => 'Lut',
	'LBL_MAR' => 'Mar',
	'LBL_APR' => 'Kwi',
	'LBL_MAY' => 'Maj',
	'LBL_JUN' => 'Cze',
	'LBL_JUL' => 'Lip',
	'LBL_AUG' => 'Sie',
	'LBL_SEP' => 'Wrz',
	'LBL_OCT' => 'Paź',
	'LBL_NOV' => 'Lis',
	'LBL_DEC' => 'Gru',
	
	'LBL_ALL_DAY' => 'Wszystkie dni',
	
	'Mobile Call' => 'Mobile Call',
	//End
	
	//Fixing colors for Shared Calendar and My Calendar
	'JS_CALENDAR_VIEW_COLOR_UPDATED_SUCCESSFULLY' => 'Zobacz kalendarz kolor zaktualizowane',
	'JS_CALENDAR_VIEW_DELETE_CONFIRMATION' => 'Czy na pewno chcesz usunąć ten pogląd kalendarz?',
	'JS_CALENDAR_VIEW_ADDED_SUCCESSFULLY' => 'Kalendarz Zobacz pomyślnie dodana',
	'JS_CALENDAR_VIEW_DELETED_SUCCESSFULLY' => 'Kalendarz Zobacz pomyślnie usunięty',
	'JS_NO_CALENDAR_VIEWS_TO_ADD' => 'Nie Kalendarz Zobacz dodać',
	'JS_EDIT_CALENDAR' => 'Edit Calendar',
);
